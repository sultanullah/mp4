package mp4;
import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;
import java.util.Set;
// This is just for illustrating the use of the two iterators provided.
public class TestMain {
	/**
	 * @param args no arguments needed.
	 */
	public static void main(String[] args) throws IOException {
		
		// Construct the graph and queue
		MovieGraph ex_graph = new MovieGraph();
		LinkedList<Movie> movie_q = new LinkedList<Movie>();
		//System.out.println(ex_graph.vertexList);
		
		// Movie Iterator, begin adding movies to the graph.
		MovieIterator iter = new MovieIterator("data/u.item.txt");
		while ( iter.hasNext() ) {
			
			// Add the movie into the graph
			Movie movie1 = iter.getNext();						// load the next movie object.
			ex_graph.addVertex(movie1);							// add that movie object into the graph.
			movie_q.add(movie1);								// make a queue of the movies iteration later.
		
		}
		
		// Make a set of current movies to be used for future calculations
		Set<Movie> movie_set = ex_graph.getVertices().keySet();
		
		// Now make a graph for the ratings
		Map<Integer, Map<Integer,Integer>> rating_movies = new HashMap<Integer, Map<Integer,Integer>>();
			
		// Set up iterator for the ratings
		// Iterate through the ratings and add them to a separate ratings graph.
		RatingIterator iter2 = new RatingIterator("data/u.data.txt");
		
		while (iter2.hasNext()) {
			Rating rating = iter2.getNext();
			
			if ( rating_movies.containsKey(rating.getMovieId())){
				
				rating_movies.get(rating.getMovieId()).put(rating.getUserId(), rating.getRating());
				
			}
			else {			
				Map<Integer,Integer> rating_ratings = new HashMap<Integer,Integer>();
				rating_ratings.put(rating.getUserId(), rating.getRating());
				rating_movies.put(rating.getMovieId(), rating_ratings);
			}
		}
			
		// Now the ratings database is created, ordered like so:
		// < movie_id , < user_id , rating > >
		
		// Iterate through the MovieGraph
		for ( Movie graph_it : ex_graph.getVertices().keySet()){
			
			LinkedList<Movie> movie_q2 = (LinkedList<Movie>) movie_q.clone();				// Make a clone of the movie_q since our implementation involves a nested iteration, using the same queue is not possible.
			
			Movie movie1 = graph_it;														// load the current movie for further iteration.
			
			
			// For each iteration of the Movies inside MovieGraph
			// We will iterate through the other Movie Queue we made, calcualting an edgeweight and adding an edge between the two.
			// After an edge is added, we drop the current movie from the queue.
			while(movie_q2.isEmpty() != true){
				
				Movie movie2 = movie_q2.peek();
				
				// If the current movie compared to the movie in the queue is the same, we aren't going to add an edge, so skip it.
				if ( movie1.equals(movie2)){
					movie_q2.poll();
					break;
				}
				
				// Set up list of likers/dislikers
				// This is preparation for edgeweight calculation.
				List<Integer> m1_likers = new ArrayList<Integer>();
				List<Integer> m1_dislikers = new ArrayList<Integer>();
				List<Integer> m2_likers = new ArrayList<Integer>();
				List<Integer> m2_dislikers = new ArrayList<Integer>();
				List<Integer> m1_review = new ArrayList<Integer>();
				List<Integer> m2_review = new ArrayList<Integer>();
				
				// Iterate through our ratings graph to fetch the ratings for these movies
				for(Map.Entry<Integer,Integer> ratingEntry : rating_movies.get(movie1.hashCode()).entrySet()){
					
					if ( ratingEntry.getValue() == 0 | ratingEntry.getValue() == 1 | ratingEntry.getValue() == 2 ){
						m1_dislikers.add(ratingEntry.getKey());
						m1_review.add(ratingEntry.getKey());
					}
					
					if ( ratingEntry.getValue() == 4 | ratingEntry.getValue() == 5){
						m1_likers.add(ratingEntry.getKey());
						m1_review.add(ratingEntry.getKey());
					}
					
				}
				
				for(Map.Entry<Integer,Integer> ratingEntry : rating_movies.get(movie2.hashCode()).entrySet()){
									
					if ( ratingEntry.getValue() == 0 | ratingEntry.getValue() == 1 | ratingEntry.getValue() == 2 ){
						m2_dislikers.add(ratingEntry.getKey());
						m2_review.add(ratingEntry.getKey());
					}
					
					if ( ratingEntry.getValue() == 4 | ratingEntry.getValue() == 5){
						m2_likers.add(ratingEntry.getKey());
						m2_review.add(ratingEntry.getKey());
					}
					
				}
				
				// Calculate intersections between likers, dislikers and total reviewers of the current two movies.
				boolean a = m1_review.retainAll(m2_review);
				boolean b = m1_likers.retainAll(m2_likers);
				boolean c = m1_dislikers.retainAll(m2_dislikers);
				int edgeweight = 1 + (m1_review.size()) - m1_likers.size() + m1_dislikers.size();
				System.out.print("Edge between " + movie1.getName() + " and " + movie2.getName() + " is " + edgeweight + "\n");
				ex_graph.addEdge(movie1, movie2, edgeweight);	// add the edge
				
				
				movie_q2.poll();	// remove head movie from the current queue, just a part of the iteration method. This one iterates through the remaining movies to edge with.				
			}			
			movie_q.poll();			// remove head movie from the current queue. This one iterates through the main movie being edged.
		}
	}	
}