package mp4;

public class Movie {
	private final int id;
	private final String name;
	private final int releaseYear;
	private final String imdbUrl;

	/**
	 * Create a new Movie object with the given information.
	 * 
	 * @param id
	 *            the movie id
	 * @param name
	 *            the name of the movie
	 * @param releaseYear
	 *            the year of the movie's release
	 * @param imdbUrl
	 *            the movie's IMDb URL
	 */
	public Movie(int id, String name, int releaseYear, String imdbUrl) {
		this.id = id;
		this.name = name;
		this.releaseYear = releaseYear;
		this.imdbUrl = imdbUrl;
	}

	/**
	 * Return the name of the movie
	 * 
	 * @return movie name
	 */
	public String getName() {
		return name;
	}
	/**
	 * Return the year of the movie
	 * 
	 * @return movie name
	 */
	public int getYear() {
		return releaseYear;
	}
	
	/**
	 * hashCode for equality testing
	 */
	@Override
	public int hashCode() {
		return id;
	}
	
	/**
	 * Method to check if two Movie objects are equal. Checks
	 * if the object is of type movie and then send it to the
	 * equals method for movie objects
	 * 
	 */
	@Override
	public boolean equals(Object other) {
		// TODO: Implement this method correctly.
		// TODO: Improve the specification for this method.
		// can only be equal if object is movie
		if(other instanceof Movie) {
			//pass off to movie version
			return equals((Movie)other);
		}
		return false; // this should be changed
	}			
	/**
	 *Each movie has a unique id, therefore the id of two movies
	 *are analyzed when determining whether two movie objects are 
	 *equal
	 */
	public boolean equals(Movie movie) {
		if (movie == null) {
			return false;
		}
		// do comparisons ...
		if(this.id == movie.id){
			return true;
		} else {
			return false;
		}
	}	
}