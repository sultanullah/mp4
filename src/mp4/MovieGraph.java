package mp4;

import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;
import java.util.Queue;

// TODO: Implement this class that represents an undirected graph with movies as vertices.
// The edges are weighted.
// This graph should be immutable except for the addition of vertices and edges. 
// It should not be possible to change a vertex after it has been added to the graph.

// You should indicate what the representation invariants and the abstraction function are for the representation you choose.

/*abstraction function: AF : R → A
* the space of representation values in the MovieGraph class is movie objects created using the Movie class which are 
* each mapped to the abstract concept of a vertex on a graph. The abstract graph is represented by a map which holds 
* all the vertices currently on the graph object. 
*/

/*representation invariant: RI : R → boolean
 * When performing operations on movies in the MovieGraph class, they must be a non-null vertices 
 * on the abstract graph. Edges between two movies must be positive and undirected.
 */

public class MovieGraph {

	//A map that holds all the vertices and their edges to other vertices
	private Map<Movie, Map<Movie,Integer>> vertexMap = new HashMap<Movie, Map<Movie,Integer>>();
	
	/**
	 *  
	 * returns a map of all the movies currently on the graph, each movie
	 * is mapped to another map that contains all the other movies that are connected to it, 
	 * along with with an edge weight.
	 * 
	 * @return the map of the graph.
	 * 
	 */	
	public Map<Movie, Map<Movie, Integer>> getVertices() {
		Map<Movie, Map<Movie, Integer>> temp = new HashMap<Movie, Map<Movie,Integer>>();
		temp.putAll(vertexMap);
		return temp;
	}
	
	/**
	 * Add a new movie to the graph. If the movie already exists in the graph
	 * then this method will return false. Otherwise this method will add the
	 * movie to the graph and return true.
	 * 
	 * @param movie
	 *            the movie to add to the graph. Requires that movie != null.
	 * @return true if the movie was successfully added and false otherwise.
	 * @modifies this by adding the movie to the graph if the movie did not
	 *           exist in the graph.
	 */
	public boolean addVertex(Movie movie) {
		// TODO: Implement this method
		//iterate through all the movies on the graph
		ArrayList<Movie> tempList = new ArrayList<Movie>();
		tempList.addAll(vertexMap.keySet());
		for(int i = 0; i < tempList.size(); i++){
			//returns false if movie already exists on the graph
			if(tempList.get(i).equals(movie)){
				return false;
			}
		}
		//return false if the movie is null
		if(movie == null){
			return false;
		} else {
			//adds the movie onto map of vertices
			vertexMap.put(movie, new HashMap<Movie,Integer>());
			return true;
		}
	}
	/**
	 * Add a new edge to the graph. If the edge already exists in the graph then
	 * this method will return false. Otherwise this method will add the edge to
	 * the graph and return true.
	 * 
	 * @param movie1
	 *            one end of the edge being added. Requires that m1 != null.
	 * @param movie2
	 *            the other end of the edge being added. Requires that m2 !=
	 *            null. Also require that m1 is not equal to m2 because
	 *            self-loops are not meaningful in this graph.
	 * @param edgeWeight
	 *            the weight of the edge being added. Requires that edgeWeight >
	 *            0.
	 * @return true if the edge was successfully added and false otherwise.
	 * @modifies this by adding the edge to the graph if the edge did not exist
	 *           in the graph.
	 */
	public boolean addEdge(Movie movie1, Movie movie2, int edgeWeight) {
		// TODO: Implement this method
		//if the edge already exists on the graph, return false
		if((vertexMap.get(movie1).containsKey(movie2)) && (vertexMap.get(movie2).containsKey(movie1))){
			return false;							
		}
		//if either movie is null or the edge weight is negative return false
		if((movie1 == null) || (movie2 == null) || (movie1.equals(movie2)) || (edgeWeight < 0)){
			return false;
		} else {
			//add the edge to the vertex map and return true
			//add the map value keys and value for both movies because the
			//graph is undirected
			vertexMap.get(movie1).put(movie2, edgeWeight);
			vertexMap.get(movie2).put(movie1, edgeWeight);
			return true;
		}
	}
	
	/**
	 * Add a new edge to the graph. If the edge already exists in the graph then
	 * this method should return false. Otherwise this method should add the
	 * edge to the graph and return true.
	 * 
	 * @param movieId1
	 *            the movie id for one end of the edge being added. Requires
	 *            that m1 != null.
	 * @param movieId2
	 *            the movie id for the other end of the edge being added.
	 *            Requires that m2 != null. Also require that m1 is not equal to
	 *            m2 because self-loops are not meaningful in this graph.
	 * @param edgeWeight
	 *            the weight of the edge being added. Requires that edgeWeight >
	 *            0.
	 * @return true if the edge was successfully added and false otherwise.
	 * @modifies this by adding the edge to the graph if the edge did not exist
	 *           in the graph.
	 */
	public boolean addEdge(int movieId1, int movieId2, int edgeWeight) {
		// TODO: Implement this method
		//if the movie id's are the same, return false
		if((movieId1 == movieId2) || (edgeWeight < 0)){
			return false;
		}
		//using a helper method to find the movies corresponding to the movie id's
		Movie movie1 = this.getMovie(movieId1);
		Movie movie2 = this.getMovie(movieId2);
		//using the other method of addEdge with movie parameters to create the edge
		return (this.addEdge(movie1, movie2, edgeWeight));
	}
	/**
	 * Return the length of the shortest path between two movies in the graph.
	 * Throws an exception if the movie ids do not represent valid vertices in
	 * the graph.
	 * 
	 * @param moviedId1
	 *            the id of the movie at one end of the path.
	 * @param moviedId2
	 *            the id of the movie at the other end of the path.
	 * @param movieId1 
	 * @throws NoSuchMovieException
	 *             if one or both arguments are not vertices in the graph.
	 * @throws NoPathException
	 *             if there is no path between the two vertices in the graph.
	 * 
	 * @return the length of the shortest path between the two movies
	 *         represented by their movie ids.
	 */
	public int getShortestPathLength(int moviedId1, int moviedId2)
			throws NoSuchMovieException, NoPathException {
		// TODO: Implement this method
		
		//determine the movies that correspond to the movie id
		Movie movie1 = getMovie(moviedId1);
		Movie movie2 = getMovie(moviedId2);
		
		//if either movie is null throw a NoSuchMovieException
		if(movie1 == null || movie2 == null){
			throw new NoSuchMovieException();
		}
	
		//create a map to store all the previous values of the movie
		//this is needed when determining the previous values of the target
		Map<Movie,Movie> previousMap = new HashMap<Movie, Movie>();
		
		//create a map to store the distance of each movie to the source(movie1)
		Map<Movie,Integer> distanceMap = new HashMap<Movie, Integer>();
		
		//iterate through the list of vertices(movies) on the graph and assign 
		//every movie with a value of infinity(maximum integer value) on the distance
		//map and a value of null on the previous map initially
		ArrayList<Movie> tempMovieList = new ArrayList<Movie>();
		tempMovieList.addAll(vertexMap.keySet());
		for(int i = 0; i < tempMovieList.size(); i++){
			previousMap.put(tempMovieList.get(i), null);
			distanceMap.put(tempMovieList.get(i), Integer.MAX_VALUE);
		}
		
		//update the source to have a value of 0 on the distance map, this represents
		//that the distance from the source to the source is zero
		distanceMap.put(movie1, 0);
		
		//create a queue to poll through all the vertices from the source to the target
		Queue<Movie> Queue = new LinkedList<Movie>();
	  	Queue.add(movie1);
	  	
	  	while (!Queue.isEmpty()) {
	  		
	  		//iterate through each neighbour of the current vertex and find the neighbor
	  		//with the smallest edge
	  		//this is adapted from Dijkstras algorithm
	  		Movie U = Queue.poll();
	  		ArrayList<Movie> neighborsOfU = new ArrayList<Movie>();
	  		neighborsOfU.addAll(vertexMap.get(U).keySet());
	  		for(int k = 0; k < neighborsOfU.size(); k++){
	  			Movie V = neighborsOfU.get(k);
	 			int length = vertexMap.get(U).get(V);
	 			int alt = distanceMap.get(U) + length;
	 			if(alt < distanceMap.get(V)){
	 				
	 				//if the target movie is encountered, break from the while loop
	 				if(U == movie2){
	 					break;
	 				}
	 			Queue.remove(V);
	 			
	 			//set the current vertex on the pervious and distance maps
	 			distanceMap.put(V, alt);
	 			previousMap.put(V, U);
	 			Queue.add(V);
	 			}
	  		}
	  	}
	  	//if the target has not been updated on the distance map(still contains a value of
	  	//infinity) then a NoPathExpection() is thrown
	  	if(distanceMap.get(movie2) == Integer.MAX_VALUE){
	  		throw new NoPathException();
	  	}
	  	
	  	//returns the distance of the target from the source contained in the distance map
	  	return distanceMap.get(movie2);
	  	}	
	/**
	 * Return the shortest path between two movies in the graph. Throws an
	 * exception if the movie ids do not represent valid vertices in the graph.
	 * 
	 * @param moviedId1
	 *            the id of the movie at one end of the path.
	 * @param moviedId2
	 *            the id of the movie at the other end of the path.
	 * @throws NoSuchMovieException
	 *             if one or both arguments are not vertices in the graph.
	 * @throws NoPathException
	 *             if there is no path between the two vertices in the graph.
	 * 
	 * @return the shortest path, as a list, between the two movies represented
	 *         by their movie ids. This path begins at the movie represented by
	 *         movieId1 and ends with the movie represented by movieId2.
	 */
	public List<Movie> getShortestPath(int movieId1, int movieId2)
			throws NoSuchMovieException, NoPathException {
		// TODO: Implement this method
		
		//same implementation as the getShortestPathLength() method, but the list of
		//previous movies are return
		
		Movie movie1 = getMovie(movieId1);
		Movie movie2 = getMovie(movieId2);
		if(movie1 == null || movie2 == null){
			throw new NoSuchMovieException();
		}
		Map<Movie,Movie> previousMap = new HashMap<Movie, Movie>();
		Map<Movie,Integer> distanceMap = new HashMap<Movie, Integer>();
		ArrayList<Movie> tempMovieList = new ArrayList<Movie>();
		tempMovieList.addAll(vertexMap.keySet());
		for(int i = 0; i < tempMovieList.size(); i++){
			previousMap.put(tempMovieList.get(i), null);
			distanceMap.put(tempMovieList.get(i), Integer.MAX_VALUE);
		}
		distanceMap.put(movie1, 0);
		Queue<Movie> vertexQueue = new LinkedList<Movie>();
	  	vertexQueue.add(movie1);
	  	while (!vertexQueue.isEmpty()) {
	  		Movie U = vertexQueue.poll();
	  		ArrayList<Movie> neighborsOfU = new ArrayList<Movie>();
	  		neighborsOfU.addAll(vertexMap.get(U).keySet());
	  		for(int k = 0; k < neighborsOfU.size(); k++){
	  			Movie V = neighborsOfU.get(k);
	 			int length = vertexMap.get(U).get(V);
	 			int alt = distanceMap.get(U) + length;
	 			if(alt < distanceMap.get(V)){
	 				if(U == movie2){
	 					break;
	 				}
	 			vertexQueue.remove(V);
	 			distanceMap.put(V, alt);
	 			previousMap.put(V, U);
	 			vertexQueue.add(V);
	 			}
	  		}
	  	}
	  	if(distanceMap.get(movie2) == Integer.MAX_VALUE){
	  		throw new NoPathException();
	  	}
	  	
	  	//list to be returned that holds all the previous values
	  	List<Movie> shortestPath = new ArrayList<Movie>();
	
	  	//if the value in the previous map is not null, add this movie to the list
	  	Movie movie = movie2;
	  	while(movie != null){
	  		shortestPath.add(movie);
	  		movie = previousMap.get(movie);
	   	}
	  	
	  	//the list will now be from the target to the source therefore it is necessary to
	  	//reverse the list so that it is from the source to the target
	  	Collections.reverse(shortestPath);
		
	  	//return the list
	  	return shortestPath;
	}
	/**
	 *  
	 * Returns the movie id given the name of the movie. For movies that are not
	 * in English, the name contains the English transliteration original name
	 * and the English translation. A match is found if any one of the two
	 * variations is provided as input. Typically the name string has <English
	 * Translation> (<English Transliteration>) for movies that are not in
	 * English.
	 * 
	 * @param name
	 *            the movie name for the movie whose id is needed.
	 * @return the id for the movie corresponding to the name. If an exact match
	 *         is not found then return the id for the movie with the best match
	 *         in terms of translation/transliteration of the movie name.
	 * @throws NoSuchMovieException
	 *             if the name does not match any movie in the graph.
	 */
	public int getMovieId(String name) throws NoSuchMovieException {
		// TODO: Implement this method
		
		//iterate through all the movies in the graph
		ArrayList<Movie> temp = new ArrayList<Movie>();
		ArrayList<Integer> movieidlist = new ArrayList<Integer>();
		temp.addAll(vertexMap.keySet());
		for(int i = 0; i < temp.size(); i++){
			
			//if the movie name is the same as a movie on the list
			//get the movie id of that movie(using hashCode())
			if(temp.get(i).getName().equals(name)){
				movieidlist.add(temp.get(i).hashCode());			
			}
		}
		//if no movies found that match the name, NoSuchMovieException() is thrown
		if(movieidlist.isEmpty()){
			throw new NoSuchMovieException();
		} else {
			//return the one movie found
			//do not have to think about multiple movies having the same id because they are unique
			return movieidlist.get(0);
		}
	}
	
	// Implement the next two methods for completeness of the MovieGraph ADT

	
	@Override
	public boolean equals(Object other) {
		// TODO: Implement this
		if(other instanceof MovieGraph) {
			//pass off to movie version
			return equals((MovieGraph)other);
		}
		return false; // this should be changed
	}	
	public boolean equals(MovieGraph moviegraph) {
		if (moviegraph == null) {
			return false;
		}
		// do comparisons ...
		if(this.hashCode() == moviegraph.hashCode()){
			return true;
		} else {
			return false;
		}
	}
	@Override
	public int hashCode() {
		// TODO: Implement a reasonable hash code method
		int foo = 0;
		int temp = 0;
		for ( Movie key : vertexMap.keySet() ){
			if (temp == 0){
				foo = foo + (key.getYear() * 36);
				temp++;
			}
			if (temp == 1){
				foo = foo + (key.getYear() * 12);
				temp--;
			}
		}		
		return foo - vertexMap.size();
	}
	
	/**
	 * returns the movie corresponding to the movieId
	 * 
	 * @param movieId
	 *            Movie Id of a movie in the graph. Requires that movie != null.
	 * @return movie containing that movie Id.
	 * 
	 */
	public Movie getMovie(int movieId) {
		//iterate through the list of movies in the graph and find the movie from
		//the movie id
		ArrayList<Movie> temp = new ArrayList<Movie>();
		ArrayList<Movie> movielist = new ArrayList<Movie>();
		temp.addAll(vertexMap.keySet());
		for(int i = 0; i < temp.size(); i++){
			if(temp.get(i).hashCode() == movieId){
				movielist.add(temp.get(i));			
			}
		}
			if(movielist.isEmpty()){
				return null;
			} else {
				return movielist.get(0);
			}
		}
	}