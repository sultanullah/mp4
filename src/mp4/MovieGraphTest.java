package mp4;

import static org.junit.Assert.*;
import java.io.IOException;
import org.junit.Test;
import java.util.ArrayList;
import java.util.List;



//_________________________________________________________
//
//	These are isolated tests.
//	Test involving the datasets are in TestMain.java
//
//_________________________________________________________

// TODO: You should implement suitable JUnit tests to verify that your implementation of the MovieGraph class is correct.

public class MovieGraphTest {
	
	// This class will trivially test functions in MovieGraph.java	
	
	// Construct the graph
	MovieGraph ex_graph = new MovieGraph();
	
	// Construct a test movie
	Movie test_movie = new Movie(1,"Chan Story", 2014, "www.ChanStory.com");
	
	// Construct a second test movie
	Movie test_movie2 = new Movie(2,"Chan Revelations", 2099, "www.ChanLegacy.com");
	
	// Construct a third test movie
	Movie test_movie3 = new Movie(3,"Chan Beautification", 6969, "www.TrueBeauty.com");
	
	
	
	
	@Test
	public void testAddVertex() throws IOException {
		
		assertEquals(true,ex_graph.addVertex(test_movie));							// Did it succeed in adding the test movie into the graph?
		assertEquals(true,ex_graph.getVertices().containsKey(test_movie));				// Does the graph now contain this movie?
		
	}
	
	@Test
	public void testAddEdge_objects() throws IOException {
		
		ex_graph.addVertex(test_movie);												// add movie1 into the graph
		ex_graph.addVertex(test_movie2);											// add movie2 into the graph
		
		int testedge = 1;												// make a sample edgeweight of one
		
		assertEquals(true,ex_graph.addEdge(test_movie, test_movie2, testedge));		// Test if our addEdge(movie objects) come out true
		
		int hold = ex_graph.getVertices().get(test_movie).get(test_movie2);				// Find the edgeweight between test_movie and test_movie2.
		assertEquals(1,hold);														// the edgeweight should = 1 as we set earlier.

		
	}
	
	@Test
	public void testAddEdge_id() throws IOException {
		
		ex_graph.addVertex(test_movie);												// add movie1 into the graph
		ex_graph.addVertex(test_movie2);											// add movie2 into the graph
		
		int testedge = 1;															// make a sample edgeweight of one
		
		assertEquals(true,ex_graph.addEdge(test_movie.hashCode(), test_movie2.hashCode(), testedge));		// Test if our addEdge(movie id) come out true
		
		int hold = ex_graph.getVertices().get(test_movie).get(test_movie2);				// Find the edgeweight between test_movie and test_movie2.
		assertEquals(1,hold);														// the edgeweight should = 1 as we set earlier.
		
		
	}
	
	@Test
	public void testShortestPathLength() throws IOException, NoSuchMovieException, NoPathException {
		
		
		// Add movies to the graph
		ex_graph.addVertex(test_movie);
		ex_graph.addVertex(test_movie2);
		ex_graph.addVertex(test_movie3);

		// Add edges to each movie, each one now has 2 edges for all permuatations.
		ex_graph.addEdge(test_movie, test_movie2, 2);
		ex_graph.addEdge(test_movie, test_movie3, 5);
		ex_graph.addEdge(test_movie2, test_movie3, 2);
		
		// By Dijkstra�s algorithm, say we take the case, from test_movie to test_movie3.
		// From movie1 to movie3, it takes 5 units.
		// From movie1 to movie2, it takes 2 units.
		// From movie2 to movie3, it takes 2 units.
		// We can go from movie1 directly to movie3 (5 units), however going from movie1 to movie2 to movie3 is a total of 4 units, which is less.
		// So the shortest path length should be 4, and not 5.
		
		assertEquals(4, ex_graph.getShortestPathLength(test_movie.hashCode(), test_movie3.hashCode()));
		
	}
	
	@Test
	public void testShortestPath() throws IOException, NoSuchMovieException, NoPathException {
		
		// Add movies to the graph
		ex_graph.addVertex(test_movie);
		ex_graph.addVertex(test_movie2);
		ex_graph.addVertex(test_movie3);

		// Add edges to each movie, each one now has 2 edges for all permuatations.
		ex_graph.addEdge(test_movie, test_movie2, 2);
		ex_graph.addEdge(test_movie, test_movie3, 5);
		ex_graph.addEdge(test_movie2, test_movie3, 2);
		
		// By Dijkstra�s algorithm, say we take the case, from test_movie to test_movie3.
		// From movie1 to movie3, it takes 5 units.
		// From movie1 to movie2, it takes 2 units.
		// From movie2 to movie3, it takes 2 units.
		// We can go from movie1 directly to movie3 (5 units), however going from movie1 to movie2 to movie3 is a total of 4 units, which is less.
		// So the shortest path should be a list, <movie1,movie3>
		
		List<Movie> shortestpath = new ArrayList<Movie>();
		shortestpath.add(test_movie);
		shortestpath.add(test_movie2);
		shortestpath.add(test_movie3);
		
		assertEquals(shortestpath, ex_graph.getShortestPath(test_movie.hashCode(), test_movie3.hashCode()));
		
	}
	
	@Test
	public void testGetMovieid() throws NoSuchMovieException {
		
		ex_graph.addVertex(test_movie);												// add test movie to the graph
		
		assertEquals(1,ex_graph.getMovieId("Chan Story"));							// get movie "Chan Story" from the graph, which should equal movie id "1"
		
	}
	
	@Test
	public void testGetMovie() throws IOException {
		
		ex_graph.addVertex(test_movie);												// add test movie to the graph
		
		assertEquals(test_movie,ex_graph.getMovie(1));								// get movie id "1" from the graph, which should equal test_movie
		
	}
	
	@Test
	public void testEquals() throws IOException {
		
		ex_graph.addVertex(test_movie);												// add movies to the first graph.
		ex_graph.addVertex(test_movie2);
		
		
		MovieGraph ex_graph2 = new MovieGraph();									// create a second graph for comparison, with the same order of movies.
		ex_graph2.addVertex(test_movie);
		ex_graph2.addVertex(test_movie2);
		
		assertEquals(ex_graph.equals(ex_graph2),true);								// both graphs should equal.
		
		MovieGraph ex_graph3 = new MovieGraph();									// create a third graph for comparison											
																					// add the movies in different order!
		ex_graph3.addVertex(test_movie);
		ex_graph3.addVertex(test_movie3);
		
		assertEquals(ex_graph.equals(ex_graph3),false);								// both graphs should not equal.

	}
	
	@Test
	public void testHashcode() throws IOException {
		
		ex_graph.addVertex(test_movie);											// add movies to the first graph.
		ex_graph.addVertex(test_movie2);
		
		
		MovieGraph ex_graph2 = new MovieGraph();								// create a second graph for comparison, with the same order of movies.
		ex_graph2.addVertex(test_movie);
		ex_graph2.addVertex(test_movie2);
		
		assertEquals(197422,ex_graph.hashCode());								// ex_graph and ex_graph2 are identical, and should have same hashcode, and by our formula, it should equal 197422
		assertEquals(197422,ex_graph2.hashCode());								// both graphs should equal.
		
		MovieGraph ex_graph3 = new MovieGraph();								// create a third graph for comparison											
		ex_graph3.addVertex(test_movie);
		ex_graph3.addVertex(test_movie3);
		
		assertEquals(431182,ex_graph3.hashCode());								// ex_graph3's hashcode should be 431182.
		
	}


}